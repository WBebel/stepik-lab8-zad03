const { createClient } = require('redis')
const { Client } = require('pg')
const express = require('express')
const app = express()
app.use(express.json())

const client = createClient({
    url: 'redis://redis:6379',
})

const pgClient = new Client({
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    host: 'postgres',
    port: 5432,
});

(async function init() {
    await pgClient.connect()
    await pgClient.query('CREATE TABLE IF NOT EXISTS users (id SERIAL PRIMARY KEY, name VARCHAR(255), email VARCHAR(255))')
})()

client.on('error', err => console.log('Redis Client Error', err))

app.get('/', async (req, res) => {
    const { key } = req.body
    await client.connect()
    const message = await client.get(key)
    res.json({ message })
    await client.disconnect()
})

app.post('/', async (req, res) => {
    const { key, message } = req.body
    await client.connect()
    await client.set(key, message)
    await client.disconnect()
    res.json({ message: 'Data saved.' })
})

app.get('/users', async (req, res) => {
    const { rows } = await pgClient.query('SELECT * FROM users')
    res.json({ users: rows })
})

app.post('/users', async (req, res) => {
    const { name, email } = req.body
    await pgClient.query('INSERT INTO users (name, email) VALUES ($1, $2)', [name, email])
    res.json({ message: 'User created.' })
})

app.listen(3000, () => console.log('Server is running on port 3000', process.env.POSTGRES_USER, process.env.POSTGRES_PASSWORD))